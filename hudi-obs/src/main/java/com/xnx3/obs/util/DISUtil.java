package com.xnx3.obs.util;

import com.huaweicloud.dis.DIS;
import com.huaweicloud.dis.DISClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.util.Properties;

/**
 * Huawei cloud DIS client util
 * @author aly
 */
public class DISUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DISUtil.class);

    // DIS client
    private static DIS dic;

    private DISUtil() {

    }

    /**
     * Obtain the client entity, and if it is not created, it will be automatically created
     * @return DIS client
     */
    public static DIS getDISClient() {
        if (dic == null) {
            synchronized (DISUtil.class) {
                // Create DIS client
                if (dic == null) {
                    dic = createDISClient();
                }
            }
        }
        return dic;
    }

    /**
     * Creating a DIS client from a configuration file
     * @return DIS client
     */
    private static DIS createDISClient() {
        try {
            /*
             * Load Configuration File
             */
            Properties prop = new Properties();
            prop.load(DISUtil.class.getClassLoader().getResourceAsStream("dis.properties"));

            /*
             * Verify configuration file parameters
             */
            if (prop.getProperty("region") == null || prop.getProperty("region").equals("")) {
                LOGGER.info("The region field in the dis.properties configuration file must be filled in");
                return null;
            }
            if (prop.getProperty("endpoint") == null || prop.getProperty("endpoint").equals("")) {
                LOGGER.info("The endpoint field in the dis.properties configuration file must be filled in");
                return null;
            }
            if (prop.getProperty("ak") == null || prop.getProperty("ak").equals("")) {
                LOGGER.info("The ak field in the dis.properties configuration file must be filled in");
                return null;
            }
            if (prop.getProperty("sk") == null || prop.getProperty("sk").equals("")) {
                LOGGER.info("The sk field in the dis.properties configuration file must be filled in");
                return null;
            }
            if (prop.getProperty("projectId") == null || prop.getProperty("projectId").equals("")) {
                LOGGER.info("The projectId field in the dis.properties configuration file must be filled in");
                return null;
            }

//            if (prop.getProperty("stream.name") != null && !prop.getProperty("stream.name").equals("")) {
//                STREAM_NAME = prop.getProperty("stream.name");
//            }
//            if (prop.getProperty("app.name") != null && !prop.getProperty("app.name").equals("")) {
//                APP_NAME = prop.getProperty("app.name");
//            }

            // Create And return
            return DISClientBuilder.standard()
                    .withRegion(prop.getProperty("region"))
                    .withEndpoint(prop.getProperty("endpoint"))
                    .withAk(prop.getProperty("ak"))
                    .withSk(prop.getProperty("sk"))
                    .withProjectId(prop.getProperty("projectId"))
                    .build();
        } catch (FileNotFoundException e) {
            LOGGER.error("Config file dis.properties not found",
                    e.getMessage(),
                    e);
            return null;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

}
