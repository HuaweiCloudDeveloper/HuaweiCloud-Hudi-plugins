package com.xnx3.obs.util;

import com.huaweicloud.dis.iface.data.response.Record;

/**
 * DIS data format processing tool
 * @author aly
 */
public class DISRecordUtil {

    /**
     * DIS data processing into Hudi available formats
     * @param record Single record of DIS event notification
     * @return The json string required by Hudi
     */
    public static String convertToString(Record record) {
        return ("{\"record\" : "
                + new String(record.getData().array())
                + ", \"partitionKey\" : \"" + record.getPartitionKey()
                + "\", \"sequenceNumber\" : \"" + record.getSequenceNumber()
                + "\", \"approximateArrivalTimestamp\" : \"" + record.getApproximateArrivalTimestamp()
                + "\", \"timestamp\" : \"" + record.getTimestamp()
                + "\", \"timestampType\" : \"" + record.getTimestampType()
                + "\"}").replaceAll("-", "_");
    }

}
