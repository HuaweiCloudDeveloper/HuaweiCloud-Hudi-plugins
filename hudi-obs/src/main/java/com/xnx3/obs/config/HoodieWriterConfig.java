package com.xnx3.obs.config;

import org.apache.spark.sql.SaveMode;

import java.util.HashMap;
import java.util.Map;

/**
 * Hudi operation parameter configuration
 * @author aly
 */
public class HoodieWriterConfig {

    private String tableName;   // Table name for the datasource write
    private String basePath;    // Base path for the datasource write
    private String keyGenerator;    // Key generator class, that implements org.apache.hudi.keygen.KeyGenerator
    private SaveMode saveMode;  // Hudi save mode. Overwrite and append
    private Map<String, String> options;    // Other options
    private String recordkeyFieldOptKey;    // Record key field. Value to be used as the recordKey component of HoodieKey
    private String precombineFieldOptKey;   // Field used in preCombining before actual write

    public HoodieWriterConfig() {
        this.options = new HashMap<String, String>();
    }

    public HoodieWriterConfig tableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public HoodieWriterConfig basePath(String basePath) {
        this.basePath = basePath;
        return this;
    }

    public HoodieWriterConfig keyGenerator(String keyGenerator) {
        this.keyGenerator = keyGenerator;
        return this;
    }

    public HoodieWriterConfig saveMode(SaveMode saveMode) {
        this.saveMode = saveMode;
        return this;
    }

    public HoodieWriterConfig options(Map<String, String> options) {
        this.options = options;
        return this;
    }

    public HoodieWriterConfig recordkeyFieldOptKey(String recordkeyFieldOptKey) {
        this.recordkeyFieldOptKey = recordkeyFieldOptKey;
        return this;
    }

    public HoodieWriterConfig precombineFieldOptKey(String precombineFieldOptKey) {
        this.precombineFieldOptKey = precombineFieldOptKey;
        return this;
    }

    public String tableName() {
        return tableName;
    }

    public String basePath() {
        return basePath;
    }

    public String keyGenerator() {
        return keyGenerator;
    }

    public SaveMode saveMode() {
        return saveMode;
    }

    public Map<String, String> options() {
        return options;
    }

    public String recordkeyFieldOptKey() {
        return recordkeyFieldOptKey;
    }

    public String precombineFieldOptKey() {
        return precombineFieldOptKey;
    }

}
