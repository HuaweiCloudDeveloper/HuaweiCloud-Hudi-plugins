package com.xnx3.obs.config;

/**
 * DIS operation parameter configuration
 * @author aly
 */
public class DISReaderConfig {

    private String streamName;  // Huawei cloud DIS channel name
    private String partitionId; // Data download partition ID
    private String startingSequenceNumber;  // Data download serial number
    private String cursorType;  // Data download type
    private Long timestamp; // Start timestamp, effective when cursorType is AT_TIMESTAMP

    public DISReaderConfig streamName(String streamName) {
        this.streamName = streamName;
        return this;
    }

    public DISReaderConfig partitionId(String partitionId) {
        this.partitionId = partitionId;
        return this;
    }

    public DISReaderConfig startingSequenceNumber(String startingSequenceNumber) {
        this.startingSequenceNumber = startingSequenceNumber;
        return this;
    }

    public DISReaderConfig cursorType(String cursorType) {
        this.cursorType = cursorType;
        return this;
    }

    public DISReaderConfig timestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String streamName() {
        return streamName;
    }

    public String partitionId() {
        return partitionId;
    }

    public String startingSequenceNumber() {
        return startingSequenceNumber;
    }

    public String cursorType() {
        return cursorType;
    }

    public Long timestamp() {
        return timestamp;
    }

}
