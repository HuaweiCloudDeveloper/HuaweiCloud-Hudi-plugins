package com.xnx3.obs.sink;

import com.xnx3.obs.config.HoodieWriterConfig;
import com.xnx3.obs.util.SparkUtil;
import org.apache.hudi.QuickstartUtils;
import org.apache.hudi.keygen.NonpartitionedKeyGenerator;
import org.apache.spark.sql.SaveMode;

import java.io.IOException;
import java.util.List;

/**
 * Hudi write to OBS demo
 * @author aly
 */
public class TestHoodieObsSink {

    public static void main(String[] args) throws IOException {
        // Configure Hudi parameters
        HoodieWriterConfig config = new HoodieWriterConfig()
                .basePath("obs://hudi-test-target/hudi_trips_cow")
                .tableName("hudi_trips_cow")
                .saveMode(SaveMode.Append)
                .keyGenerator(NonpartitionedKeyGenerator.class.getName())
                .recordkeyFieldOptKey("ts")
                .precombineFieldOptKey("ts");

        // Create 10 pieces of data
        QuickstartUtils.DataGenerator dataGenerator = new QuickstartUtils.DataGenerator();
        List<String> inserts = QuickstartUtils.convertToStringList(dataGenerator.generateInserts(10));

        // Write to OBS
        new HoodieObsSink().writeHudi(SparkUtil.getSparkSession(), config, inserts);

        // Stop spark Running
        SparkUtil.getSparkSession().stop();
    }

}
