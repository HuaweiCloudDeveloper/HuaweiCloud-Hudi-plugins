
## OBS Filesystem

这个页面描述了如何让你的Hudi spark任务使用HuaweiCloud OBS存储。

## HuaweiCloud OBS 部署

为了让Hudi使用OBS，需要增加两部分的配置：

- 为Hudi增加HuaweiCloud OBS的相关配置
- 增加Jar包的MVN依赖

## HuaweiCloud OBS 相关的配置

新增下面的配置到你的Hudi能访问的core-site.xml文件。使用你的OBS bucket name替换掉`fs.defaultFS`中的`bucketname`，使用OBS endpoint地址替换掉`fs.obs.endpoint`，使用OBS的key和secret分别替换掉`fs.obs.access.key`和`fs.obs.secret.key`。这样Hudi就能读写相应的bucket。

```xml
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>obs://bucketname/</value>
    </property>
    <property>
        <name>fs.obs.access.key</name>
        <value>Huawei Cloud access key</value>
    </property>
    <property>
        <name>fs.obs.secret.key</name>
        <value>Huawei Cloud secret key</value>
    </property>
    <property>
        <name>fs.obs.endpoint</name>
        <value>Huawei Cloud OBS endpoint to connect to</value>
    </property>
    <property>
        <name>fs.obs.impl</name>
        <value>org.apache.hadoop.fs.obs.OBSFileSystem</value>
    </property>
</configuration>
```
## HuaweiCloud OBS Libs

将HuaweiCloud Hadoop库、HuaweiCloud DIS SDK、HuaweiCloud OBS SDK添加到我们的pom.xml中。

```xml
<dependency>
    <groupId>com.huaweicloud</groupId>
    <artifactId>esdk-obs-java-bundle</artifactId>
    <version>[3.21.11,)</version>
</dependency>
<dependency>
    <groupId>com.huaweicloud.dis</groupId>
    <artifactId>huaweicloud-sdk-java-dis</artifactId>
    <version>1.3.5</version>
</dependency>
<dependency>
    <groupId>org.apache.hadoop</groupId>
    <artifactId>hadoop-huaweicloud</artifactId>
    <version>3.1.1-hw-45</version>
</dependency>
```

